'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
var mongoose = require('mongoose');
var cors = require('cors');
var express=require('express');
var utils = require('./api/lib/utils');
var bodyParser = require('body-parser');
app.use(cors());
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};
//DB Connection


mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/data');

mongoose.set('debug', true);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log('Database connection successful!');
});

// app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(bodyParser.json({limit: '50mb', type: 'application/json'}));

//Check to call web services where token is not required//
app.use(express.static('public'));

app.use('/api/*', function(req, res, next) {
var freeAuthPath = [
  '/api/createuser',
  '/api/userLogin'
];
var available = false;
for (var i = 0; i < freeAuthPath.length; i++) {
if (freeAuthPath[i] == req.baseUrl) {
available = true;
break;
}
}
if (!available) {
utils.ensureAuthorized(req, res, next);
} else {
next();
}
});
SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);
  // config swagger ui

  app.use(swaggerExpress.runner.swaggerTools.swaggerUi());

  var port = process.env.PORT || 3002;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://172.10.28.3:' + port + '/hello?name=Scott');
  }
});
