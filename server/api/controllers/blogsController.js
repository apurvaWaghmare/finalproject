const blogsModel = require('../models/blogs');
const jwt = require('jsonwebtoken');
const constantObj = require('../lib/constant');
var fs = require("fs")
module.exports = {
    
    createblogs: createblogs,
    listblogs:listblogs,
    getblogsdetails:getblogsdetails,
    deleteblogs:deleteblogs,
    editblogs:editblogs

}
function createblogs(req, res) {
    console.log(req.files.file[0]);
    timeStamp = Date.now();
    originalImageName = timeStamp + "_"+req.files.file[0].originalname;
    var imagePath ='../server/public/bloguploads/'+ originalImageName;
    fs.writeFile(imagePath,(req.files.file[0].buffer),function(err){
        if (err)throw err;
        console.log("Product Image uploaded");
    })
    var user = new blogsModel();
    user.titlename = req.swagger.params.titlename.value;
    user.description = req.swagger.params.description.value;
    user.caption = req.swagger.params.caption.value;
    user.is_delete = false;
    user.image = "http://localhost:3002/bloguploads/" + originalImageName
    user.save(function(err,response){
        if (err) {
            res.json({
                code: 404,
                message: 'blogs yet to add'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully added blogs');

        }

    })
}

function listblogs(req, res) {
    blogsModel.find({is_delete :false},function (err,data){
    console.log("listing blogs data", data);
    res.json({
        code: 200,
        message: "success",
        data: data,
        token:"token"
    });

    });
}
function getblogsdetails(req,res){
    var _id=req.swagger.params.id.value
    blogsModel.findById(_id,function(err,blogsModel){
     console.log("dataaaa",_id)
        if (err) {
            res.json({
                code: 404,
                message: 'blogs yet to add to list'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully added to blogslist');

        }

    })
}
function deleteblogs(req,res){
    var _id=req.swagger.params.id.value
    var is_delete=true
    blogsModel.findByIdAndRemove(_id,{$set:{is_delete:is_delete}},function(err,product){
        console.log("dataaaa",_id)
        if (err) {
            res.json({
                code: 404,
                message: 'blogs yet to be delete'
            })
        } else {
            res.json({
                code: 200,
                data:  product
            })
            console.log('Successfully deleted blogs ');

        }

    })
}

function editblogs(req, res) {
    var _id=req.swagger.params.id.value
    console.log(_id);
    console.log(req.files.file[0]);
    timeStamp = Date.now();
    originalImageName = timeStamp + "_"+req.files.file[0].originalname;
    var imagePath ='../admin/src/assets/images/bloguploads/'+ originalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        
        console.log("Product Image uploaded");
    })
    image= "assets/images/bloguploads/"+originalImageName
    blogsModel.findByIdAndUpdate(_id, { $set: { titlename: req.body.titlename, description: req.body.description, caption: req.body.caption, image:image, is_active: req.body.is_active, is_delete: req.body.is_delete } }, function (err, data) {
        console.log(_id);
        if (err) {
            res.json({
                code: 404,
                message: 'blogs yet to be updated'
            })
        } else {
            res.json({
                code: 200,
                data: data,
            })
            console.log('Successfully Updated blogs');

        }

    })
}