
const userModel = require('../models/user');
const jwt = require('jsonwebtoken');
const constantObj = require('../lib/constant');
module.exports = {

    createuser: createuser,
    userLogin: userLogin,
   
}

function createuser(req, res) {
    let user = new userModel({ firstname: req.body.firstname,lastname: req.body.lastname,emailaddress: req.body.emailaddress,password: req.body.password,is_active: req.body.is_active, is_delete: req.body.is_delete,
        role: "user"
    })
    user.save(user, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'admin is yet to add'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully created');

        }

    })
}
function userLogin(req, res) {
    var emailaddress = req.body.emailaddress;
    var password = req.body.password;
    userModel.findOne({
        $and: [ { emailaddress }, { password }]
    
    }, function (err, admin) {
        console.log(admin, "admin");
        if (err) {
            console.log("there is an error")

        } else {
           
            const token = jwt.sign(admin.toJSON(), 'apurva', {
                expiresIn: 1440 // expires in 1 hour
            });
            console.log("generated", token);
            
           
            res.json({
                code: 200,
                message: "success",
                data: admin,
                token:token
            });
        }


    });
}

