const productModel = require('../models/product');
const jwt = require('jsonwebtoken');
const constantObj = require('../lib/constant');
var fs = require("fs")
module.exports = {

    createproduct: createproduct,
    listproduct:listproduct,
    getproductdetails:getproductdetails,
    deleteproduct:deleteproduct,
    editproduct:editproduct

}
function createproduct(req, res) {
    console.log(req.files.file[0]);
    timeStamp = Date.now();
    originalImageName = timeStamp + "_"+req.files.file[0].originalname;
    var imagePath ='../server/public/bloguploads/'+ originalImageName;
    fs.writeFile(imagePath,(req.files.file[0].buffer),function(err){
        if (err)throw err;
        console.log("Product Image uploaded");
    })
    var user = new productModel();
    user.productname = req.swagger.params.productname.value;
    user.productcost = req.swagger.params.productcost.value;
    user.shippingcost = req.swagger.params.shippingcost.value;
    
    user.is_delete = false;
    user.image = "http://localhost:3002/bloguploads/"+originalImageName
    user.save(function(err,response){
        if (err) {
            res.json({
                code: 404,
                message: 'product yet to add'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully added product');

        }

    })
}

function listproduct(req, res) {
    productModel.find({is_delete :false},function (err,data){
    console.log("listing data", data);
    res.json({
        code: 200,
        message: "success",
        data: data,
        token:"token"
    });

    });
}
function getproductdetails(req,res){
    var_id=req.swagger.params.id.value
    productModel.findById(_id,function(err,productModel){
     console.log("dataaaa",_id)
        if (err) {
            res.json({
                code: 404,
                message: 'product yet to add to list'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully added to list');

        }

    })
}
function deleteproduct(req,res){
    var _id=req.swagger.params.id.value
    var is_delete=true
    productModel.findByIdAndRemove(_id,{$set:{is_delete:is_delete}},function(err,product){
        console.log("dataaaa",_id)
        if (err) {
            res.json({
                code: 404,
                message: 'product yet to be delete'
            })
        } else {
            res.json({
                code: 200,
                data:  product
            })
            console.log('Successfully deleted');

        }

    })
}

function editproduct(req,res){
    var _id=req.swagger.params.id.value
    console.log(_id);
    console.log(req.files.file[0]);
    timeStamp = Date.now();
    originalImageName = timeStamp + "_"+req.files.file[0].originalname;
    var imagePath ='../admin/src/assets/images/bloguploads/'+ originalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        
        console.log("Product Image uploaded");
    })
    image= "assets/images/bloguploads/"+originalImageName
    productModel.findByIdAndUpdate(_id,{$set:{productname: req.body.productname,productcost: req.body.productcost,shippingcost: req.body.shippingcost,image:image,is_active: req.body.is_active, is_delete: req.body.is_delete}},function(err,data){
        console.log(_id);
        if (err) {
            res.json({
                code: 404,
                message: 'product yet to be updated'
            })
        } else {
            res.json({
                code: 200,
                data: data
            })
            console.log('Successfully Updated');

        }

    })
}