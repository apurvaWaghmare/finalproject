const usersModel = require('../models/users');
const jwt = require('jsonwebtoken');
const constantObj = require('../lib/constant');
module.exports = {

    createusers: createusers,
    listusers:listusers,
    getusersdetails:getusersdetails,
    deleteusers:deleteusers,
    editusers:editusers 

    

}
function createusers(req, res) {
    console.log("req.body>>>>>>>", req.body)
    let user = new usersModel({ username: req.body.username,emailadd: req.body.emailadd,phoneno: req.body.phoneno,is_active: req.body.is_active, is_delete: false,
        type: "users"
    })
    user.save(user, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'product yet to add'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully added');

        }

    })
}

function listusers(req, res) {
    usersModel.find({is_delete:false},function (err,data){
    console.log("listing data", data);
    res.json({
        code: 200,
        message: "success",
        data: data,
        token:"token"
    });

    });
}
function getusersdetails(req,res){
    var_id=req.swagger.params.id.value
    usersModel.findById(_id,function(err,usersModel){
     console.log("dataaaa",_id)
        if (err) {
            res.json({
                code: 404,
                message: 'users yet to add to list'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully added to list');

        }

    })
}
function deleteusers(req,res){
    var _id=req.swagger.params.id.value
    var is_delete=true
    usersModel.findByIdAndRemove(_id,{$set:{is_delete:is_delete}},function(err,product){
        console.log("dataaaa",_id)
        if (err) {
            res.json({
                code: 404,
                message: 'product yet to be delete'
            })
        } else {
            res.json({
                code: 200,
                data:  product
            })
            console.log('Successfully deleted');

        }

    })
}

function editusers(req,res){
    var _id=req.swagger.params.id.value
    console.log(_id);
    usersModel.findByIdAndUpdate(_id,{$set:{username: req.body.username,emailadd: req.body.emailadd, phoneno: req.body.phoneno, is_active: req.body.is_active, is_delete: req.body.is_delete}},function(err,data){
        console.log(_id);
        if (err) {
            res.json({
                code: 404,
                message: 'product yet to be updated'
            })
        } else {
            res.json({
                code: 200,
                data:  data
            })
            console.log('Successfully Updated');

        }

    })
}