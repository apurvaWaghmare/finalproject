const commentModel = require('../models/comment');
const blogsModel = require('../models/blogs');
module.exports = {
    addcomment: addcomment,
    listcomment: listcomment,
    getblogsById :getblogsById
}

function addcomment(req, res) {
    var user = new commentModel();
    user.comment = req.body.comment;
    user.date = Date.now();
    user.blogid = req.body.blogid;
    user.is_delete = false;
    user.userid = req.body.userid;
    user.ratings = req.body.ratings;
    user.save(function (err, data) {
        if (err) {
            res.json({
                code: 404,
                message: 'comment not Added'
            })
        } else {
            res.json({
                code: 200,
                data: data
            })
            console.log('Successfully created');

        }

    })
}

function listcomment(req, res) {
    commentModel.find({ blogid: req.swagger.params.id.value, is_delete: false }).populate({
        path: 'blogid',
        model: 'blogsModel',
    }).populate({
        path: 'userid',
        model: 'UserModel',

    }).exec(function (err, data) {
        if(err){
            console.log(err)
        }else{
        console.log("After Listing data", data);
        res.json(data);
        }
    })
}

function getblogsById(req, res){
var _id =req.swagger.params.id.value;
blogsModel.findOne({_id:_id},function(err,data){
     console.log("dataaaa",_id , data);
        if (err) {
            console.log(err)
        } else if (data){
           res.json(data);

            }else{
            console.log(data)
            }
        
    })
}

