const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let userSchema = new mongoose.Schema({
    firstname: { type: String },
    lastname: { type: String },
    emailaddress: { type: String },
    password: { type: String },
    is_active: {type:Boolean },
    is_delete : {type: Boolean },
    role:{type:String}
 
});

const userModel = mongoose.model('UserModel', userSchema);
module.exports = userModel;
