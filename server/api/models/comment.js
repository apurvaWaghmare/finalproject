const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let commentSchema = new mongoose.Schema({
    userid: { type: String, ref:'UsersModel' },
    comment: { type: String },
    blogid : { type: String, ref:'blogsModel'},
    ratings: {type:Number},
    date: { type: Date },
    reply: { type: String },
    is_delete : {type: Boolean}

   
 
});
const commentModel = mongoose.model('commentModel', commentSchema);
module.exports = commentModel;

