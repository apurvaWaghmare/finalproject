const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let blogsSchema = new mongoose.Schema({
    titlename: { type: String },
    description: { type: String },
    caption: { type: String },
    image: { type: String },
    is_delete : {type: Boolean },

 
});

const blogsModel = mongoose.model('blogsModel', blogsSchema);
module.exports = blogsModel;
