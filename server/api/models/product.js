const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let productSchema = new mongoose.Schema({
    productname: { type: String },
    productcost: { type: Number },
    shippingcost: { type: Number },
    image: { type: String },
    is_delete : {type: Boolean },

 
});

const productModel = mongoose.model('productModel', productSchema);
module.exports = productModel;
