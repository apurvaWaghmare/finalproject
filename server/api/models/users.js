const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let usersSchema = new mongoose.Schema({
    username: { type: String },
    emailadd: { type: String },
    phoneno :  { type: Number },
    is_delete : {type: Boolean },
 
});

const usersModel = mongoose.model('UsersModel', usersSchema);
module.exports = usersModel;
